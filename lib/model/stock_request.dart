class StockRequest {
  int stockQuantity;
  int minQuantity;

  StockRequest(this.stockQuantity, this.minQuantity);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};

    data['stockQuantity'] = stockQuantity;
    data['minQuantity'] = minQuantity;

    return data;
  }
}