import 'package:dio/dio.dart';
import 'package:see_you_app/config/config_api.dart';
import 'package:see_you_app/functions/token_lib.dart';
import 'package:see_you_app/model/common_result.dart';
import 'package:see_you_app/model/stock_request.dart';

class RepoStock {
  Future<CommonResult> setStock(StockRequest request) async {
    const String baseUrl = '$apiUri/stock/data';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.post(
        baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return CommonResult.fromJson(response.data);
  }

  Future<CommonResult> putStock(StockRequest request) async {
    const String baseUrl = '$apiUri/stock/update/{id}';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.put(
        baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return CommonResult.fromJson(response.data);
  }
}