import 'package:flutter/material.dart';
import 'package:see_you_app/components/common/component_appbar_actions.dart';
import 'package:see_you_app/components/common/component_count_title.dart';
import 'package:see_you_app/components/common/component_list_textline_item.dart';
import 'package:see_you_app/pages/page_main.dart';

class PageBoardList extends StatefulWidget {
  const PageBoardList({super.key});

  @override
  State<PageBoardList> createState() => _PageBoardListState();
}

class _PageBoardListState extends State<PageBoardList> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarActions(
        title: "게시글 리스트",
      ),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    // if (_totalItemCount > 0) {
    return Container(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          const ComponentCountTitle(Icons.attachment, 100, '건', '재고'),
          ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: 100,
            itemBuilder: (_, index) => ComponentListTextLineItem(
              title: '제목',
              isUseContent1Line: true,
              content1Subject: '작성일',
              content1Text: '2023-10-10',
              voidCallback: () { Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => PageMain())); },
            ),
          )
        ],
      ),
    );
    // } else {
    //   // 이용내역이 없을때 흰화면 보이면 에러난것처럼 보이니까 이용내역 없다고 보여주기.
    //   return SizedBox(
    //     height: MediaQuery.of(context).size.height - 30 - 50 - 50 - 70,
    //     child: const ComponentNoContents(
    //       icon: Icons.history,
    //       msg: '게시글이 없습니다.',
    //     ),
    //   );
    // }
  }
}
