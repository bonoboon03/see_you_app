import 'package:flutter/material.dart';
import 'package:see_you_app/components/common/component_appbar_actions.dart';
import 'package:see_you_app/components/common/component_margin_vertical.dart';
import 'package:see_you_app/config/config_color.dart';
import 'package:see_you_app/config/config_size.dart';
import 'package:see_you_app/config/config_style.dart';

class PageMoneyHistoryListDetail extends StatefulWidget {
  const PageMoneyHistoryListDetail({super.key});

  @override
  State<PageMoneyHistoryListDetail> createState() => _PageMoneyHistoryListDetailState();
}

class _PageMoneyHistoryListDetailState extends State<PageMoneyHistoryListDetail> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarActions(
        title: "2023-10 직원명",
      ),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    return ListView(
      children: [
        Container(
          padding: const EdgeInsets.all(20),
          child: Container(
            padding: const EdgeInsets.all(15),
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(color: colorLightGray),
              borderRadius: BorderRadius.circular(buttonRadius),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text(
                      "세전금액",
                      style: TextStyle(
                        fontSize: fontSizeMid,
                      ),
                    ),
                    Text(
                      "24,000,000",
                      style: const TextStyle(
                        fontSize: fontSizeMid,
                      ),
                    ),
                  ],
                ),
                const ComponentMarginVertical(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text(
                      "국민연금",
                      style: TextStyle(
                        fontSize: fontSizeMid,
                      ),
                    ),
                    const Text(
                      "10,000",
                      style: TextStyle(
                        fontSize: fontSizeMid,
                      ),
                    ),
                  ],
                ),
                const ComponentMarginVertical(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text(
                      "건강보험",
                      style: TextStyle(
                        fontSize: fontSizeMid,
                      ),
                    ),
                    Text(
                      "10,000",
                      style: TextStyle(
                        fontSize: fontSizeMid,
                      ),
                    ),
                  ],
                ),
                const ComponentMarginVertical(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text(
                      "건강보험",
                      style: TextStyle(
                        fontSize: fontSizeMid,
                      ),
                    ),
                    Text(
                      "10,000",
                      style: const TextStyle(
                        fontSize: fontSizeMid,
                      ),
                    ),
                  ],
                ),
                const ComponentMarginVertical(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text(
                      "장기요양보험",
                      style: TextStyle(
                        fontSize: fontSizeMid,
                      ),
                    ),
                    Text(
                      "10,000",
                      style: TextStyle(
                        fontSize: fontSizeMid,
                      ),
                    ),
                  ],
                ),
                const ComponentMarginVertical(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text(
                      "고용보험",
                      style: TextStyle(
                        fontSize: fontSizeMid,
                      ),
                    ),
                    Text(
                      "10,000",
                      style: const TextStyle(
                        fontSize: fontSizeMid,
                      ),
                    ),
                  ],
                ),
                const ComponentMarginVertical(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text(
                      "산재보험",
                      style: TextStyle(
                        fontSize: fontSizeMid,
                      ),
                    ),
                    Text(
                      "10,000",
                      style: const TextStyle(
                        fontSize: fontSizeMid,
                      ),
                    ),
                  ],
                ),
                const ComponentMarginVertical(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text(
                      "소득세",
                      style: TextStyle(
                        fontSize: fontSizeMid,
                      ),
                    ),
                    Text(
                      "10,000",
                      style: const TextStyle(
                        fontSize: fontSizeMid,
                      ),
                    ),
                  ],
                ),
                const ComponentMarginVertical(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text(
                      "지방소득세",
                      style: TextStyle(
                        fontSize: fontSizeMid,
                      ),
                    ),
                    Text(
                      "10,000",
                      style: const TextStyle(
                        fontSize: fontSizeMid,
                      ),
                    ),
                  ],
                ),
                const ComponentMarginVertical(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text(
                      "총공제액",
                      style: TextStyle(
                        fontSize: fontSizeMid,
                      ),
                    ),
                    Text(
                      "10,000",
                      style: const TextStyle(
                        fontSize: fontSizeMid,
                      ),
                    ),
                  ],
                ),
                const ComponentMarginVertical(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text(
                      "급여",
                      style: TextStyle(
                        fontSize: fontSizeMid,
                      ),
                    ),
                    Text(
                      "10,000",
                      style: const TextStyle(
                        fontSize: fontSizeMid,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
